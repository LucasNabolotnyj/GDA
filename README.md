Bem Vindo ao projeto Guia do Aluno 3.0!

Para instalar e testar esse software, basta seguir os passos abaixo:

Nota: **É necessário [NodeJS](https://nodejs.org/en/), [MongoDB](https://www.mongodb.com/download-center/community) e [Git](https://git-scm.com/) instalados no computador.**

| *Passos para instalação do servidor + banco* |
| ------------ |
| Clone os arquivos para o computador: `git clone https://gitlab.com/LucasNabolotnyj/GDA.git` |
| Acesse a pasta **mongo**: `cd GDA/mongo`|
| Via linha de comando, adicione os documentos ao banco: `mongo < query.js`|
| Acesse a pasta **server** por linha de comando: `cd GDA/server` |
| Instale as dependencias do software: `npm install` |
| Execute o programa em modo de desinvolvimento: `npm run start:dev`|

Neste ponto, é possível testar a API diretamente, utilizando ou o navegador ou programas de teste de API, 
como o [Postman](https://www.getpostman.com/) (que foi utilizado neste projeto).

Seguindo, para testar os demais componentes, basta seguir via linha de comando:

| *Passos para instalação do cliente* |
| ------------ |
| Acesse a pasta **client** por linha de comando: `cd GDA/client`|
| Instale as dependencias do software: `npm install`|
| Execute o programa, seja em modo mobile (`localhost:8200`) ou normalmente (`localhost:8100`)|

Pronto! Com isso já é possível executar o programa completamente!

PS: O projeto está em estado de desinvolvimento e será atualizado constantemente durante os próximos meses.****
Este projeto faz parte da Monografia Guia do Aluno 3, disponível em https://docs.google.com/document/d/1SQg_oFY-NVJXNEe_xOAmZ82xwcEcbYBEfu_4Ftqoyz4/edit?usp=sharing
