import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'assistencia-estudantil', loadChildren: './pages/assistencia-estudantil/assistencia-estudantil.module#AssistenciaEstudantilPageModule' },
  { path: 'apoio-ingressante', loadChildren: './pages/assistencia-estudantil/apoio-ingressante/apoio-ingressante.module#ApoioIngressantePageModule' },
  { path: 'apoio-social-pedagogico', loadChildren: './pages/assistencia-estudantil/apoio-social-pedagogico/apoio-social-pedagogico.module#ApoioSocialPedagogicoPageModule' },
  { path: 'coracao-estudante', loadChildren: './pages/assistencia-estudantil/coracao-estudante/coracao-estudante.module#CoracaoEstudantePageModule' },
  { path: 'editais-praec', loadChildren: './pages/assistencia-estudantil/editais-praec/editais-praec.module#EditaisPraecPageModule' },
  { path: 'atividades-acad', loadChildren: './pages/atividades-acad/atividades-acad.module#AtividadesAcadPageModule' },
  { path: 'extensao-cultura', loadChildren: './pages/atividades-acad/extensao-cultura/extensao-cultura.module#ExtensaoCulturaPageModule' },
  { path: 'pesquisa', loadChildren: './pages/atividades-acad/pesquisa/pesquisa.module#PesquisaPageModule' },
  { path: 'cardapio', loadChildren: './pages/cardapio/cardapio.module#CardapioPageModule' },
  { path: 'campi', loadChildren: './pages/contatos/campi/campi.module#CampiPageModule' },
  { path: 'reitoria', loadChildren: './pages/contatos/reitoria/reitoria.module#ReitoriaPageModule' },
  { path: 'como-achar-site-curso', loadChildren: './pages/dicas/como-achar-site-curso/como-achar-site-curso.module#ComoAcharSiteCursoPageModule' },
  { path: 'consulta-ppc', loadChildren: './pages/dicas/consulta-ppc/consulta-ppc.module#ConsultaPpcPageModule' },
  { path: 'links-uteis', loadChildren: './pages/dicas/links-uteis/links-uteis.module#LinksUteisPageModule' },
  { path: 'editais', loadChildren: './pages/editais/editais.module#EditaisPageModule' },
  { path: 'eventos', loadChildren: './pages/eventos/eventos.module#EventosPageModule' },
  { path: 'ciencias-sem-fronteiras', loadChildren: './pages/intercambios/ciencias-sem-fronteiras/ciencias-sem-fronteiras.module#CienciasSemFronteirasPageModule' },
  { path: 'idiomas-sem-fronteiras', loadChildren: './pages/intercambios/idiomas-sem-fronteiras/idiomas-sem-fronteiras.module#IdiomasSemFronteirasPageModule' },
  { path: 'santander-bolsas-ibero-americanas', loadChildren: './pages/intercambios/santander-bolsas-ibero-americanas/santander-bolsas-ibero-americanas.module#SantanderBolsasIberoAmericanasPageModule' },
  { path: 'participacao-estudantil', loadChildren: './pages/participacao-estudantil/participacao-estudantil.module#ParticipacaoEstudantilPageModule' },
  { path: 'comissao-curso', loadChildren: './pages/participacao-estudantil/comissao-curso/comissao-curso.module#ComissaoCursoPageModule' },
  { path: 'conselho-bibliotecas', loadChildren: './pages/participacao-estudantil/conselho-bibliotecas/conselho-bibliotecas.module#ConselhoBibliotecasPageModule' },
  { path: 'conselho-de-campus', loadChildren: './pages/participacao-estudantil/conselho-de-campus/conselho-de-campus.module#ConselhoDeCampusPageModule' },
  { path: 'diretorios-centros-acad', loadChildren: './pages/participacao-estudantil/diretorios-centros-acad/diretorios-centros-acad.module#DiretoriosCentrosAcadPageModule' },
  { path: 'bib-web', loadChildren: './pages/servicos-online/bib-web/bib-web.module#BibWebPageModule' },
  { path: 'guia-tel', loadChildren: './pages/servicos-online/guia-tel/guia-tel.module#GuiaTelPageModule' },
  { path: 'moodle', loadChildren: './pages/servicos-online/moodle/moodle.module#MoodlePageModule' },
  { path: 'portal-aluno', loadChildren: './pages/servicos-online/portal-aluno/portal-aluno.module#PortalAlunoPageModule' },
  { path: 'acoes-afirmativas', loadChildren: './pages/assistencia-estudantil/acoes-afirmativas/acoes-afirmativas.module#AcoesAfirmativasPageModule' },
  { path: 'apoio-participacao-discente', loadChildren: './pages/assistencia-estudantil/apoio-participacao-discente/apoio-participacao-discente.module#ApoioParticipacaoDiscentePageModule' },
  { path: 'concorrer-beneficios', loadChildren: './pages/assistencia-estudantil/concorrer-beneficios/concorrer-beneficios.module#ConcorrerBeneficiosPageModule' },
  { path: 'cultura-esp-laz-inc', loadChildren: './pages/assistencia-estudantil/cultura-esp-laz-inc/cultura-esp-laz-inc.module#CulturaEspLazIncPageModule' },
  { path: 'plano-permanencia', loadChildren: './pages/assistencia-estudantil/plano-permanencia/plano-permanencia.module#PlanoPermanenciaPageModule' },
  { path: 'ensino', loadChildren: './pages/atividades-acad/ensino/ensino.module#EnsinoPageModule' },
  { path: 'pda', loadChildren: './pages/atividades-acad/pda/pda.module#PdaPageModule' },
  { path: 'calendario-acad', loadChildren: './pages/calendario-acad/calendario-acad.module#CalendarioAcadPageModule' },
  { path: 'contatos', loadChildren: './pages/contatos/contatos.module#ContatosPageModule' },
  { path: 'pro-reitorias', loadChildren: './pages/contatos/pro-reitorias/pro-reitorias.module#ProReitoriasPageModule' },
  { path: 'dicas', loadChildren: './pages/dicas/dicas.module#DicasPageModule' },
  { path: 'como-faz', loadChildren: './pages/dicas/como-faz/como-faz.module#ComoFazPageModule' },
  { path: 'criterios-aprovacao', loadChildren: './pages/dicas/criterios-aprovacao/criterios-aprovacao.module#CriteriosAprovacaoPageModule' },
  { path: 'login-institucional', loadChildren: './pages/dicas/login-institucional/login-institucional.module#LoginInstitucionalPageModule' },
  { path: 'estagios', loadChildren: './pages/estagios/estagios.module#EstagiosPageModule' },
  { path: 'intercambios', loadChildren: './pages/intercambios/intercambios.module#IntercambiosPageModule' },
  { path: 'experiencias-intercambios', loadChildren: './pages/intercambios/experiencias-intercambios/experiencias-intercambios.module#ExperienciasIntercambiosPageModule' },
  { path: 'racol', loadChildren: './pages/intercambios/racol/racol.module#RacolPageModule' },
  { path: 'noticias', loadChildren: './pages/noticias/noticias.module#NoticiasPageModule' },
  { path: 'comissao-campi', loadChildren: './pages/participacao-estudantil/comissao-campi/comissao-campi.module#ComissaoCampiPageModule' },
  { path: 'comissao-superior', loadChildren: './pages/participacao-estudantil/comissao-superior/comissao-superior.module#ComissaoSuperiorPageModule' },
  { path: 'conselho-curador', loadChildren: './pages/participacao-estudantil/conselho-curador/conselho-curador.module#ConselhoCuradorPageModule' },
  { path: 'conselho-universitario', loadChildren: './pages/participacao-estudantil/conselho-universitario/conselho-universitario.module#ConselhoUniversitarioPageModule' },
  { path: 'servicos-online', loadChildren: './pages/servicos-online/servicos-online.module#ServicosOnlinePageModule' },
  { path: 'gaucha', loadChildren: './pages/servicos-online/gaucha/gaucha.module#GauchaPageModule' },
  { path: 'guri', loadChildren: './pages/servicos-online/guri/guri.module#GuriPageModule' },
  { path: 'ouvidoria', loadChildren: './pages/servicos-online/ouvidoria/ouvidoria.module#OuvidoriaPageModule' },
  { path: 'tabs', loadChildren: './pages/tabs/tabs.module#TabsPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
