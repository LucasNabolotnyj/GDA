import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { CalendarioAcadPage } from './pages/calendario-acad/calendario-acad.page';
import { AssistenciaEstudantilPage } from './pages/assistencia-estudantil/assistencia-estudantil.page';
import { ParticipacaoEstudantilPage } from './pages/participacao-estudantil/participacao-estudantil.page';
import { AtividadesAcadPage } from './pages/atividades-acad/atividades-acad.page';
import { EstagiosPage } from './pages/estagios/estagios.page';
import { IntercambiosPage } from './pages/intercambios/intercambios.page';
import { ServicosOnlinePage } from './pages/servicos-online/servicos-online.page';
import { ContatosPage } from './pages/contatos/contatos.page';
import { DicasPage } from './pages/dicas/dicas.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  pages: Array<{icone:string, titulo: string, url: any}>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    // variavel para o ngFor e navegacao do menu lateral
    this.pages = [
      { icone:'assets/icon/icone_calendario_academico_cor_opt.png', titulo: 'Calendário Acadêmico', url:''},
      { icone:'assets/icon/icone_assistencia_estudantil_cor_opt.png', titulo: 'Assistência Estudantil', url:'assistencia-estudantil'},
      { icone:'assets/icon/icone_participacao_estudantil_cor_opt.png', titulo: 'Participação Estudantil', url:''},
      { icone:'assets/icon/icone_atividades_academicas_cor_opt.png', titulo: 'Atividades Acadêmcias', url:''},
      { icone:'assets/icon/icone_estagio_cor_opt.png', titulo: 'Estágios', url:''},
      { icone:'assets/icon/icone_intercambio_cor_opt.png', titulo: 'Intercâmbios', url:''},
      { icone:'assets/icon/icone_servicos_online_cor_opt.png', titulo: 'Serviços Online', url:''},
      { icone:'assets/icon/icone_contatos_cor_opt.png', titulo: 'Contatos', url:''},
      { icone:'assets/icon/icone_dicas_cor_opt.png', titulo: 'Dicas', url:''}
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
