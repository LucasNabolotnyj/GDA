import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  constructor(private httpClient: HttpClient) { }

  getStringAUniversidade():Observable<any> {
     return this.httpClient.get('http://localhost:8888/database/5da85db7d309c98bab8f88dd');
  }
}
