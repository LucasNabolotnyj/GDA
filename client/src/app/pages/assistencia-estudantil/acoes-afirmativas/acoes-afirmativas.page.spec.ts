import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcoesAfirmativasPage } from './acoes-afirmativas.page';

describe('AcoesAfirmativasPage', () => {
  let component: AcoesAfirmativasPage;
  let fixture: ComponentFixture<AcoesAfirmativasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcoesAfirmativasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcoesAfirmativasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
