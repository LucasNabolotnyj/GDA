import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApoioParticipacaoDiscentePage } from './apoio-participacao-discente.page';

describe('ApoioParticipacaoDiscentePage', () => {
  let component: ApoioParticipacaoDiscentePage;
  let fixture: ComponentFixture<ApoioParticipacaoDiscentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApoioParticipacaoDiscentePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApoioParticipacaoDiscentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
