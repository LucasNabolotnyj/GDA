import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ApoioSocialPedagogicoPage } from './apoio-social-pedagogico.page';

const routes: Routes = [
  {
    path: '',
    component: ApoioSocialPedagogicoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ApoioSocialPedagogicoPage]
})
export class ApoioSocialPedagogicoPageModule {}
