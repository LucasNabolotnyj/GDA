import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AssistenciaEstudantilPage } from './assistencia-estudantil.page';

const routes: Routes = [
  {
    path: '',
    component: AssistenciaEstudantilPage,
    children: [
      {
            path: 'concorrer-beneficios',
            children: [
                {
                  path: '',
                  loadChildren: () =>
                  import('./concorrer-beneficios/concorrer-beneficios.module').then(m => m.ConcorrerBeneficiosPageModule)
                }
              ]    
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AssistenciaEstudantilPage]
})
export class AssistenciaEstudantilPageModule {}
