import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssistenciaEstudantilPage } from './assistencia-estudantil.page';

describe('AssistenciaEstudantilPage', () => {
  let component: AssistenciaEstudantilPage;
  let fixture: ComponentFixture<AssistenciaEstudantilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssistenciaEstudantilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssistenciaEstudantilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
