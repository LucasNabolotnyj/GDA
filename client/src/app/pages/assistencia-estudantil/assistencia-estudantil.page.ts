import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/database/database.service';

@Component({
  selector: 'app-assistencia-estudantil',
  templateUrl: './assistencia-estudantil.page.html',
  styleUrls: ['./assistencia-estudantil.page.scss'],
})
export class AssistenciaEstudantilPage implements OnInit {
  public retornos: any[];


  constructor(private dbService: DatabaseService) {
  }


  ngOnInit() {
    try {
      this.dbService.getStringAUniversidade().subscribe((res) => { this.retornos = res });
    } catch (err) {
      console.log(err);
    }
  }

}