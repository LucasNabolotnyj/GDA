import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcorrerBeneficiosPage } from './concorrer-beneficios.page';

describe('ConcorrerBeneficiosPage', () => {
  let component: ConcorrerBeneficiosPage;
  let fixture: ComponentFixture<ConcorrerBeneficiosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConcorrerBeneficiosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcorrerBeneficiosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
