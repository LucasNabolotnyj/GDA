import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CulturaEspLazIncPage } from './cultura-esp-laz-inc.page';

const routes: Routes = [
  {
    path: '',
    component: CulturaEspLazIncPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CulturaEspLazIncPage]
})
export class CulturaEspLazIncPageModule {}
