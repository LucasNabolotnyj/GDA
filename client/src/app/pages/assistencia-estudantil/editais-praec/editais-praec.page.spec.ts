import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditaisPraecPage } from './editais-praec.page';

describe('EditaisPraecPage', () => {
  let component: EditaisPraecPage;
  let fixture: ComponentFixture<EditaisPraecPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditaisPraecPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditaisPraecPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
