import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtividadesAcadPage } from './atividades-acad.page';

describe('AtividadesAcadPage', () => {
  let component: AtividadesAcadPage;
  let fixture: ComponentFixture<AtividadesAcadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtividadesAcadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtividadesAcadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
