import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProReitoriasPage } from './pro-reitorias.page';

const routes: Routes = [
  {
    path: '',
    component: ProReitoriasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProReitoriasPage]
})
export class ProReitoriasPageModule {}
