import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProReitoriasPage } from './pro-reitorias.page';

describe('ProReitoriasPage', () => {
  let component: ProReitoriasPage;
  let fixture: ComponentFixture<ProReitoriasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProReitoriasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProReitoriasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
