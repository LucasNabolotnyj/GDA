import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReitoriaPage } from './reitoria.page';

describe('ReitoriaPage', () => {
  let component: ReitoriaPage;
  let fixture: ComponentFixture<ReitoriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReitoriaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReitoriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
