import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CriteriosAprovacaoPage } from './criterios-aprovacao.page';

const routes: Routes = [
  {
    path: '',
    component: CriteriosAprovacaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CriteriosAprovacaoPage]
})
export class CriteriosAprovacaoPageModule {}
