import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginInstitucionalPage } from './login-institucional.page';

describe('LoginInstitucionalPage', () => {
  let component: LoginInstitucionalPage;
  let fixture: ComponentFixture<LoginInstitucionalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginInstitucionalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginInstitucionalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
