import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExperienciasIntercambiosPage } from './experiencias-intercambios.page';

const routes: Routes = [
  {
    path: '',
    component: ExperienciasIntercambiosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExperienciasIntercambiosPage]
})
export class ExperienciasIntercambiosPageModule {}
