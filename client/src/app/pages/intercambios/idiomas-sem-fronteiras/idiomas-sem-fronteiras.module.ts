import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IdiomasSemFronteirasPage } from './idiomas-sem-fronteiras.page';

const routes: Routes = [
  {
    path: '',
    component: IdiomasSemFronteirasPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IdiomasSemFronteirasPage]
})
export class IdiomasSemFronteirasPageModule {}
