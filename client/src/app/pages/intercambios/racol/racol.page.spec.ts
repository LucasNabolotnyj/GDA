import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RacolPage } from './racol.page';

describe('RacolPage', () => {
  let component: RacolPage;
  let fixture: ComponentFixture<RacolPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RacolPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RacolPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
