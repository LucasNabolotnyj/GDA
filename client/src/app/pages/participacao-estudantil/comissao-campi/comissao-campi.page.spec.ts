import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissaoCampiPage } from './comissao-campi.page';

describe('ComissaoCampiPage', () => {
  let component: ComissaoCampiPage;
  let fixture: ComponentFixture<ComissaoCampiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissaoCampiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissaoCampiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
