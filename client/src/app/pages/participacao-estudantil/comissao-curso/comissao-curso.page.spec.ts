import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissaoCursoPage } from './comissao-curso.page';

describe('ComissaoCursoPage', () => {
  let component: ComissaoCursoPage;
  let fixture: ComponentFixture<ComissaoCursoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComissaoCursoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissaoCursoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
