import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ComissaoSuperiorPage } from './comissao-superior.page';

const routes: Routes = [
  {
    path: '',
    component: ComissaoSuperiorPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ComissaoSuperiorPage]
})
export class ComissaoSuperiorPageModule {}
