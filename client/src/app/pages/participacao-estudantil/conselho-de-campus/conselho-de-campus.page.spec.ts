import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConselhoDeCampusPage } from './conselho-de-campus.page';

describe('ConselhoDeCampusPage', () => {
  let component: ConselhoDeCampusPage;
  let fixture: ComponentFixture<ConselhoDeCampusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConselhoDeCampusPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConselhoDeCampusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
