import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DiretoriosCentrosAcadPage } from './diretorios-centros-acad.page';

const routes: Routes = [
  {
    path: '',
    component: DiretoriosCentrosAcadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DiretoriosCentrosAcadPage]
})
export class DiretoriosCentrosAcadPageModule {}
