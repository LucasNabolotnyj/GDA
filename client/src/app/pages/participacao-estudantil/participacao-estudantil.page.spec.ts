import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipacaoEstudantilPage } from './participacao-estudantil.page';

describe('ParticipacaoEstudantilPage', () => {
  let component: ParticipacaoEstudantilPage;
  let fixture: ComponentFixture<ParticipacaoEstudantilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipacaoEstudantilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipacaoEstudantilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
