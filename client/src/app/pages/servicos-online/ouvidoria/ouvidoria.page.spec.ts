import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OuvidoriaPage } from './ouvidoria.page';

describe('OuvidoriaPage', () => {
  let component: OuvidoriaPage;
  let fixture: ComponentFixture<OuvidoriaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OuvidoriaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OuvidoriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
