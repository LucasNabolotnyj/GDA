import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'noticias',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../noticias/noticias.module').then(m => m.NoticiasPageModule)
          }
        ]
      },
      {
        path: 'editais',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../editais/editais.module').then(m => m.EditaisPageModule)
          }
        ]
      },
      {
        path: 'eventos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../eventos/eventos.module').then(m => m.EventosPageModule)
          }
        ]
      },
      {
        path: 'cardapio',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../cardapio/cardapio.module').then(m => m.CardapioPageModule)
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs',
    pathMatch: 'full',
    component: TabsPage
  
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
