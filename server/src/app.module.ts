import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { DatabaseService } from './database/database.service';


@Module({
  imports: [MongooseModule.forRoot('mongodb://localhost:27017/gda'),
   DatabaseModule
  ],
  controllers: [AppController,],
  providers: [AppService,],
})
export class AppModule {}
