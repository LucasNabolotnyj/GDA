import { Test, TestingModule } from '@nestjs/testing';
import { StringsController } from './database.controller';

describe('Strings Controller', () => {
  let controller: StringsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StringsController],
    }).compile();

    controller = module.get<StringsController>(StringsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
