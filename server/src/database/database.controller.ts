import { Controller, Post, Get, Res, Body, HttpStatus, Param, NotFoundException } from '@nestjs/common';
import { DatabaseService } from './database.service';
import { StringsDTO } from './database.dto';

@Controller('database')
export class StringsController {
    constructor(private readonly DatabaseService: DatabaseService) { }

    // add a String
    @Post('/create')
    async addString(@Res() res, @Body() AssitenciaEDTO: StringsDTO) {
        const StringDB = await this.DatabaseService.addString(AssitenciaEDTO);
        return res.status(HttpStatus.OK).json({
            message: "String Criada Com Sucesso",
            StringDB
        })
    }

    // Retrieve StringDBs list
    @Get('strings')
    async getAllStringDB(@Res() res) {
        const StringDBs = await this.DatabaseService.getAllStrings();
        return res.status(HttpStatus.OK).json(StringDBs);
    }

    // Fetch a particular StringDB using ID
    @Get(':StringDBID')
    async getStringDB(@Res() res, @Param('StringDBID') StringDBID) {
        const StringDB = await this.DatabaseService.getString(StringDBID);
        if (!StringDB) throw new NotFoundException('String does not exist!');
        return res.status(HttpStatus.OK).json(StringDB);
    }
}