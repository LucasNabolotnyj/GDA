export class StringsDTO {
    readonly page: string;
    readonly content:[{
        readonly id:string;
        readonly value:string;
    }]
}