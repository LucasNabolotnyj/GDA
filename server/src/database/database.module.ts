import { Module } from '@nestjs/common';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { StringSchema } from './database.schema'
import { StringsController } from './database.controller';
import { DatabaseService } from './database.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'String', schema: StringSchema }]),
  ],
  controllers: [StringsController],
  providers: [DatabaseService]
})

export class DatabaseModule { }
