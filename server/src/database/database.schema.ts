import * as mongoose from 'mongoose';

export const AEConstSchema = new mongoose.Schema({    
    id: String,
    value: String
})

export const StringSchema = new mongoose.Schema({
    page:String,
    content:[AEConstSchema]
})