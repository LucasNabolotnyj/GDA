import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Strings } from './database.interface';
import { StringsDTO } from './database.dto'
@Injectable()
export class DatabaseService {

    constructor(@InjectModel('String') private readonly AEModel: Model<Strings>) { }
    // pega todas as Strings
    async getAllStrings(): Promise<Strings[]> {
        const stringsAE = await this.AEModel.find().exec();
        return stringsAE;
    }
    // pega String especifica
    async getString(StringID): Promise<Strings> {
        const stringsAE = await this.AEModel.findById(StringID).exec();
        return stringsAE;
    }
    // salva nova String
    async addString(AEsDTO: StringsDTO): Promise<Strings> {
        const newString = await this.AEModel(AEsDTO);
        return newString.save();
    }
    // Edita String existente
    async updateString(StringID, AEsDTO: StringsDTO): Promise<Strings> {
        const updatedString = await this.AEModel
            .findByIdAndUpdate(StringID, AEsDTO, { new: true });
        return updatedString;
    }
    // deleta String especifica
    async deleteString(StringID): Promise<any> {
        const deletedString = await this.AEModel.findByIdAndRemove(StringID);
        return deletedString;
    }
}